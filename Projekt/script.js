let counter = 0;
let upgradesCounter = 1;
let timeUpgrade = 0;
let perClick = 1;
let perSec = 0;
let firstCost = 5;

const counterWrapper = document.querySelector(".counter");
const btn = document.querySelector(".click");
const upgrade = document.querySelector(".first-upgrade");
const counterPerClick = document.querySelector(".perClick");
const counterPerSec = document.querySelector(".perSec");
const costOfPlusOne = document.querySelector(".firstUpgradeCost");

function updateCounter() {
  counterWrapper.innerHTML = Math.round(counter);
  counterPerClick.innerHTML = perClick;
  counterPerSec.innerHTML = Math.round(perSec * 100) / 100;
  costOfPlusOne.innerHTML = Math.round(firstCost);
}

window.setInterval(function() {
  counter = counter + timeUpgrade * 1;
  updateCounter();
}, 1000);

btn.addEventListener("click", function() {
  counter = counter + 1 * upgradesCounter;
  updateCounter();
});

upgrade.addEventListener("click", function() {
  if(counter>= firstCost) {
    counter = counter - firstCost;
    firstCost = firstCost * 1.3;
    timeUpgrade = timeUpgrade + 0.1;
    perSec = perSec + 0.1;
    updateCounter();
  } else {
    alert('za mało punktów, potrzebujesz więcej xD');
  }
});















